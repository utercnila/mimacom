# MoviePoster

La parte front de la aplicación

## Despliegue de la aplicación

Se puede arrancar con `ng serve` y acceder a la url `http://localhost:4200/` o se puede construir el contenedor con docker:
`docker build -t mimacom .` y una vez construido ejecutarlo con `docker run -d -it -p 4200:80 mimacom`

## Pruebas de test

Ejecutar `ng test` para ejecutar las pruebas con [Karma]

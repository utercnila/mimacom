import {Component, OnInit} from '@angular/core';
import {CartService} from '../../../services/cart/cart.service';
import * as events from 'events';

@Component({
  selector: 'app-cart-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  cart = [];
  totalToPay = 0;

  constructor(private cartService: CartService) {
  }

  ngOnInit(): void {
    this.cart = this.cartService.cart;

    Object.values(this.cart).forEach(value => {
      this.totalToPay += (value.price / 1000 * value.qty);
    });
  }

  updateValue(product, type, event: any) {
    let qty = event.target.value;
    switch (type) {
      case 0:
        Object.values(this.cart).forEach(value => {
          if (value.id == product.id) {
            value.qty -= 1;
            if (value.qty == 0) {
              let foundGeneralCart = this.cartService.cart.findIndex(x => x.id == product.id);
              this.cartService.cart.splice(foundGeneralCart, 1);
            }
          }
        });
        break;
      case 1:
        Object.values(this.cart).forEach(value => {
          if (value.id == product.id) {
            value.qty += 1;
          }
        });
        break;
      case 2:
        if (qty == 0) {
          let foundGeneralCart = this.cartService.cart.findIndex(x => x.id == product.id);
          let foundCart = this.cart.findIndex(x => x.id == product.id);
          this.cartService.cart.splice(foundGeneralCart, 1);
          this.cart.splice(foundCart, 1);
        }
        Object.values(this.cart).forEach(value => {
          if (value.id == product.id) {
            value.qty = event.target.value;
          }
        });
        break;

    }
    this.totalToPay = 0;
    Object.values(this.cart).forEach(value => {
      this.totalToPay += (value.price / 1000 * value.qty);
    });
  }

}

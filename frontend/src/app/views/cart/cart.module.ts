import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartRoutingModule } from './cart.routing';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CartRoutingModule
  ]
})
export class CartModule { }

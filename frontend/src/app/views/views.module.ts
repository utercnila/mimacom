import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthGuard } from '../auth/auth.guard';
import { ViewsRouting } from './views.routing';
import { HomeComponent } from './home/home.component';
import { ProductListComponent } from './product/list/product-list.component';
import { ProductFavouriteComponent } from './product/favourite/product-favourite.component';
import { ProductImportComponent } from './product/import/product-import.component';
import { CartComponent } from './cart/cart.component';
import { DetailComponent } from './cart/detail/detail.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [HomeComponent, ProductListComponent, ProductFavouriteComponent, ProductImportComponent, CartComponent, DetailComponent],
  imports: [
    CommonModule,
    ViewsRouting
  ],
  exports: [HomeComponent, ProductListComponent, ProductFavouriteComponent, ProductImportComponent],
  providers: [AuthGuard, HttpClientModule]
})
export class ViewsModule { }

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductFavouriteComponent } from './product-favourite.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('FavouriteComponent', () => {
  let component: ProductFavouriteComponent;
  let fixture: ComponentFixture<ProductFavouriteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ ProductFavouriteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductFavouriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

});

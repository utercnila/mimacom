import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductListComponent } from './list/product-list.component';
import { ProductFavouriteComponent } from './favourite/product-favourite.component';
import { ProductImportComponent } from './import/product-import.component';

const routes: Routes = [
  {path: '', component: ProductListComponent},
  {path: 'list', component: ProductListComponent},
  {path: 'favourite', component: ProductFavouriteComponent},
  {path: 'import', component: ProductImportComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }

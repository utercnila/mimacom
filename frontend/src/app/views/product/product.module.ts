import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductRoutingModule } from './product.routing';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ProductRoutingModule
  ],
  exports: []
})
export class ProductModule { }

import {Component, OnInit} from '@angular/core';
import { ProductService } from '../../../services/product/product.service';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { increment } from '../../../store/actions';
import { CartService } from '../../../services/cart/cart.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  productsTotal = [];
  actual_page$ = new Observable<number>();
  page = 0;

  constructor(private productService: ProductService,
              private cartService: CartService,
              private store: Store<{ count: number }>)
  {
    this.actual_page$ = store.select('count');
  }

  ngOnInit(): void {
    this.productService.getAllProducts(0).subscribe(res => {
      this.productsTotal = res.products;
    });
  }

  loadMoreProducts() {
    this.store.dispatch(increment());

    this.actual_page$.subscribe(res => this.page = res);

    this.productService.getAllProducts(this.page).subscribe(res => {
      Object.values(res.products).forEach(value => {
        this.productsTotal.push(value);
      });
    });
  }

  loadProducts() {
    this.productService.getAllProducts(this.page).subscribe(res => {
      this.productsTotal = res.products;
    });
  }

  addToFavourite(product) {
    let objIndex = this.productsTotal.findIndex((obj => obj.id == product.id));
    if(product.favourite) {
      this.productsTotal[objIndex].favourite = true;
      product.favourite = true;
    } else {
      this.productsTotal[objIndex].favourite = false;
      product.favourite = false;
    }

    this.productService.setFavouriteProduct(product).subscribe(() => {
        this.loadProducts();
    });

  }

  addToCart(product) {

    let foundIndex = this.cartService.cart.findIndex(x => x.id == product.id);
    if(foundIndex == -1) {
      product.qty = 0;
      product.qty += 1;
      this.cartService.cart.push(product);
    } else{
      this.cartService.cart[foundIndex].qty += 1;
    }

  }


}

import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';

export interface ProductImportData {
  crawled_at: string;
  description: string;
  title: string;
  url: string;
  poster: string;
  raw_description: string;
  produced_by: string;
  directed_by: string;
}

export interface ProductImportResponse {
  status: string;
  data: ProductImportData;
}
export const httpHeaders = {
  withCredentials: true,
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

class ProductResponse {
}

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) { }

  getAllProducts(page = 0): any {
    return this.httpClient.get(this.apiUrl + '/product?page=' + page);
  }

  getAllFavouriteProducts(page = 0, favourite = 1): any {
    return this.httpClient.get(this.apiUrl + '/product?page=' + page + '&favourite=true');
  }

  importProducts(): any {
    let products;
    this.httpClient.get('https://www.fakerestapi.com/datasets/api/v1/movie-details-dataset.json')
      .subscribe( (response: ProductImportResponse) => {
          if (response.status == 'ok') {
            products = response.data;
          }
        this.httpClient.post(this.apiUrl + '/product/import', products, httpHeaders).subscribe();
        });


    return products;
  }

  setFavouriteProduct(product) {
    return this.httpClient.put(this.apiUrl + '/product/setFavourite/' + product.id, {favourite: product.favourite}, httpHeaders);
  }
}

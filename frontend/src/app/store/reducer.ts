import { createReducer, on } from '@ngrx/store';
import { increment } from './actions';

export const initialState = 0;

const incReducer = createReducer(
  initialState,
  on(increment, (state) => state + 1)
);

export function reducer(state, action) {
  return incReducer(state, action);
}

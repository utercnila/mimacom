import { createAction } from '@ngrx/store';

export const increment = createAction('[Page] Increment');

export const environment = {
  production: true,
  adminRoot: '/app',
  apiUrl: 'https://local.baristapp.com/api',
};

package com.alin.mimacom.controller;

import com.alin.mimacom.entity.Product;
import com.alin.mimacom.repository.ProductRepository;
import com.fasterxml.jackson.databind.util.JSONPObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.crypto.Data;
import java.util.*;


@RestController
@RequestMapping("/product")
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600,
        allowedHeaders="*", allowCredentials = "true")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    /* Update an customer */
    @PutMapping("/setFavourite/{id}")
    public ResponseEntity<?> update(@RequestBody HashMap<String, Object> favourite, @PathVariable Integer id) throws Exception {
        try {
            boolean newFav;
            Optional<Product> product = productRepository.findById(id);

            newFav = !(boolean) favourite.get("favourite");

            product.get().setFavourite(newFav);
            return ResponseEntity.status(HttpStatus.OK).body(productRepository.save(product.get()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(("{\"error\": \"" + e.getMessage() + "\"}"));
        }
    }

    /* Save all products */
    @PostMapping("/import")
    public ResponseEntity<?> importProducts(@RequestBody List<
            Map<String, Object>> products) throws Exception {
        try {

            List<Product> addProducts = new ArrayList<>();
            Random random = new Random();
            int min = 0, max = 40;
            boolean[] favourite = {true, false};

            for (int i = 0; i < products.size(); i++) {
                Product newProduct = new Product();
                newProduct.setImage_url((String)products.get(i).get("poster"));
                newProduct.setName((String)products.get(i).get("title"));
                newProduct.setDescription((String)products.get(i).get("description"));
                newProduct.setStock(random.nextInt(300));
                newProduct.setPrice((int)((min + (random.nextDouble() * (max - min)) * 1000)));
                newProduct.setFavourite(favourite[random.nextInt(favourite.length)]);

                addProducts.add(newProduct);
            }
            productRepository.saveAll(addProducts);

            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(("{\"error\": \"" + e.getMessage() + "\"}"));
        }
    }

    /* Get all products */
    @GetMapping
    public ResponseEntity<Map<String, Object>> readAll(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "false") boolean favourite,
            @RequestParam(defaultValue = "10") int size
    ) throws Exception {
        Map<String, Object> response = new HashMap<>();
        try {
            List<Product> products = new ArrayList<>();
            Pageable paging = PageRequest.of(page, size);

            Page<Product> pageProducts = null;

            if(favourite) {
                pageProducts = productRepository.findAllByFavourite(paging, true);
            } else {
                pageProducts = productRepository.findAll(paging);
            }

            products = pageProducts.getContent();

            response.put("products", products);
            response.put("currentPage", pageProducts.getNumber());
            response.put("totalItems", pageProducts.getTotalElements());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

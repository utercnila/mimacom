package com.alin.mimacom.service;

import com.alin.mimacom.entity.Product;
import com.alin.mimacom.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public class ProductService implements ProductInterface{

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Page<Product> findAll(Pageable page) throws Exception{
        try {
            return productRepository.findAll(page);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
    @Override
    public Page<Product> findAllByFavourite(Pageable page, boolean favourite) throws Exception {
        try {
            return productRepository.findAllByFavourite(page, favourite);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public Optional<Product> findById(Integer id) throws Exception {
        try {
            return productRepository.findById(id);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public Product save(Product product) throws Exception{
        try {
            return productRepository.save(product);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
    @Override
    @Transactional
    public void saveAll(List<Product> products) throws Exception{
        try {
            productRepository.saveAll(products);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
}

package com.alin.mimacom.service;

import com.alin.mimacom.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ProductInterface {

    public Page<Product> findAll(Pageable page) throws Exception;
    Page<Product> findAllByFavourite(Pageable page, boolean favourite) throws Exception;
    public Optional<Product> findById(Integer id) throws Exception;
    public Product save(Product product) throws Exception;
    public void saveAll(List<Product> products) throws Exception;
}

package com.alin.mimacom.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "product", catalog = "mimacom", schema = "")
public class Product{

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String image_url;

    @Column
    private Integer stock;

    @Column
    private String name;

    @Column
    private Integer price;

    @Column
    private String description;

    @Column
    private boolean favourite;

    public Product() {

    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    public Product(String image_url, Integer stock, String name, Integer price, String description, boolean favourite) {
        this.image_url = image_url;
        this.stock = stock;
        this.name = name;
        this.price = price;
        this.description = description;
        this.favourite = favourite;
    }
}

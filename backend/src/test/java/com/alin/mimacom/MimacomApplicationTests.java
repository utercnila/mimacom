package com.alin.mimacom;

import com.alin.mimacom.repository.ProductRepository;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@SpringBootTest
class MimacomApplicationTests {

    @Autowired
    private ProductRepository productRepository;
    @Test
    @Order(1)
    public void getProducts() {
        System.out.println(productRepository.findAll().size());
    }
    @Test
    @Order(2)
    public void getProductsFavourites() throws Exception {
        Pageable paging = PageRequest.of(0, 100);
        System.out.println(productRepository.findAllByFavourite(paging, true));
    }

}
